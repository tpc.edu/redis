[TOC]

# Redis高级

> **Redis**是一种**基于KV键值**的**支持多种数据结构、持久化、事务及主从复制**的**高性能**的非关系型数据库(**NoSQL**)；  
> **NoSQL**是什么 有了MySQL为什么又出现了它, **Redis数据类型**, **Redis持久化**, Redis事务, **Redis主从复制**, Redis缓存失效机制 (在第一个视频里)
**Redis6新数据类型**, **Redis秒杀**, **Redis集群**, **Redis应用问题-缓存穿透,缓存击穿,缓存雪崩**, **Redis应用问题-分布式锁**, **Lua**, Redis6新功能及总结 (在第二个视频里)

## 学习资料
* [Redis 进阶](https://www.bilibili.com/video/BV1oW411u75R)
* [Redis 6 进阶](https://www.bilibili.com/video/BV1Rv41177Af)
* [官网](https://redis.io/)
* [中文官网](http://www.redis.cn/)
* [思维导图](docs/尚硅谷Redis课程思维导图.xmind)
* [思维导图](docs/尚硅谷Redis课程思维导图_周阳.mmap)
* [学习笔记](https://www.yuque.com/yingwenerjie/obg6ll/fn23m8)
* [学习笔记](https://www.yuque.com/bingtanghulusi/za9wmf/sagqr9)
* [Redis命令](http://doc.redisfans.com/index.html)
* [Redis命令](http://www.redis.cn/commands.html)

## 1.MySQL高级内容介绍

![MySQL](docs/img/Redis.png "Redis高级内容介绍")
